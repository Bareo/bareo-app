# bareo-app Makefile to replace grunt which suffers from dependency hell

build: bareo-app docs

docs:
	docco src/js/models/*.js -o docs/models/
	docco src/js/views/*.js -o docs/views/
	docco src/js/*.js -o docs/

clean:
	rm -rf static/bareo-app.js
	rm -rf static/bareo-app.min.js
	rm -rf static/bareo-app.css
	rm -rf static/bareo-app.min.css
	rm -rf docs/models/*
	rm -rf docs/views/*

bareo-app: bareo-app.min.js bareo-app.css

bareo-app.min.js: bareo-app.js
	uglifyjs --compress --screw-ie8 -o static/bareo-app.min.js -- static/bareo-app.js

bareo-app.js:
	cat src/js/lib/underscore.js \
	src/js/lib/jquery-2.1.4.js \
	src/js/lib/backbone.js \
	src/js/lib/backbone-relational.js \
	src/js/lib/bootstrap.js \
	src/js/config.js \
	src/js/models/*.js \
	src/js/views/*.js \
	src/js/bootstrap.js > static/bareo-app.js

bareo-app.css: bootstrap.css
	cat static/bootstrap.css \
	static/bootstrap-theme.css \
	static/app.css \
	static/ltmenu.css > static/bareo-app.css

bootstrap.css:
	lessc src/less/bootstrap.less static/bootstrap.css


