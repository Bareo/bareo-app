/**
 * Bareo web client JavaScript bootstrapping file
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

if (window.location.search == "") {
	var app = new AppView;
	
	// Wrap Backbone.sync so that we can add our custom X-Authorization header
	var orig_sync = Backbone.sync;
	Backbone.sync = function(method, model, options) {
		options || (options = {});
		options.headers = {"X-Authorization": app.user.get("auth_token") || " "};
		return orig_sync(method, model, options);
	};
} else {
	var token = window.location.search.split('=').pop();
	if (token) {
		var hidden_input = document.getElementById("token_input");
		hidden_input.setAttribute("value",token);
	}

	$("#email-confirmation-form").submit(function(e) {
		console.log(e);
		e.preventDefault();
		e.stopPropagation();

		var confirm_email_url = Config.baseURL + "auth/confirm/email";
		var xhr = Config.createCORSRequest('POST', confirm_email_url);
		var body = '{"password":"'+document.getElementById('password_input').value+'","username":"'+document.getElementById('username_input').value+'","auth_token":"'+token+'"}';

		xhr.onload = function() {
			window.location = "index.html";
		};

		xhr.send(body);		
	});
	
	$("#password-reset-form").submit(function(e) {
		console.log(e);
		e.preventDefault();
		e.stopPropagation();
		
		var passwd = document.getElementById("password_input").value;
		var passwd_conf = document.getElementById("password_confirm").value;
		if (passwd === passwd_conf) {
			var reset_url = Config.baseURL + "auth/reset";
			var xhr = Config.createCORSRequest('POST', reset_url);
			var body = '{"password":"'+passwd+'","auth_token":"'+token+'"}';
			
			xhr.onload = function() {
				window.location = "index.html";
			};
			
			xhr.send(body);
		}
	});
}
