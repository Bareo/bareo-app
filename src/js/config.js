/**
 * Bareo web client JavaScript bootstrapping file
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var Config = {
	baseURL:  "https://api.bareo.io/v2/",
	devURL:   "http://api.bareo.hiphop/v2/",
	localURL: "http://localhost:8080/v2/",

	appURL: "https://app.bareo.io/",

	createCORSRequest: function(method, url) {
		var xhr = new XMLHttpRequest();

		if ("withCredentials" in xhr) {
			// XHR for Chrome/Firefox/Opera/Safari.
			xhr.open(method, url, true);
			xhr.setRequestHeader('Content-Type','application/json');
		} else if (typeof XDomainRequest != "undefined") {
			// XDomainRequest for IE.
			xhr = new XDomainRequest();
			xhr.open(method, url);
			xhr.setRequestHeader('Content-Type','application/json');
		} else {
			// CORS not supported.
			xhr = null;
		}
		return xhr;
	}
};

// Uncomment the following line to point bareo-app at a non-production
// API instance
//Config.baseURL = Config.localURL;
//Config.baseURL = Config.devURL;

