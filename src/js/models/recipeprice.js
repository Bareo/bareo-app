/**
 * RecipePrice BackboneJS model for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var RecipePrice = Backbone.RelationalModel.extend({
	idAttribute: "cid",
	
	defaults: {
		quantity: 0.0,
		price: 0.0
	},

	url: function() {
		return false;
	}
});

var RecipePrices =  Backbone.Collection.extend({
	model: RecipePrice,
	url: function() {
		return false;
	}
});
