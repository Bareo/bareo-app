/**
 * Account BackboneJS model for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGBPLv3 or later
 *
 **/

var Account = Backbone.RelationalModel.extend({
	idAttribute: "id",

	// An Account has many "User" child nodes
	relations: [{
		type: Backbone.HasMany,
		key: "users",
		relatedModel: "User",
		reverseRelation: {
			key: "account_id",
			includeInJSON: "id"
		}
	},{
		type: Backbone.HasMany,
		key: "items",
		relatedModel: "Item",
		reverseRelation: {
			key: "account_id",
			includeInJSON: "id"
		}
	},{
		type: Backbone.HasMany,
		key: "recipes",
		relatedModel: "Recipe",
		reverseRelation: {
			key: "account_id",
			includeInJSON: "id"
		}
	}],

	defaults: {
		account_name: "",
		payment_token: "",
		allowed: 1,
		account_type: 0
	},

	urlRoot: '/accounts/'
});

// Definition for a Backbone Collection of Account models
var Accounts = Backbone.Collection.extend({
	model: Account,
	url: "/accounts/"
});
