/**
 * Sale BackboneJS model for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var Sale = Backbone.RelationalModel.extend({
	idAttribute: "id",

	/*relations: [{
		type: Backbone.HasMany,
		key: "items",
		relatedModel: "Item",
		reverseRelation: {
			key: "sale_id",
			includeInJSON: "id"
		}
	},{
		type: Backbone.HasMany,
		key: "recipes",
		relatedModel: "Recipe",
		reverseRelation: {
			key: "sale_id",
			includeInJSON: "id"
		}
	}],*/

	defaults: {
		name: "",
		status: 0,
		order_date: "",
		finish_date: "",
		price: 0.00,
		material_cost: 0.00,
		account_id: 0,
		objects: {
			items: {},
			recipes: {}
		}
	},

	url: function() {
		return Config.baseURL + "sales/" + this.id;
	}
});

var Sales = Backbone.Collection.extend({
	model: Sale,
	url: function() {
		return Config.baseURL + "sales/";
	}
});
