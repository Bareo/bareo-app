/**
 * Stock BackboneJS model for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 */

var Stock = Backbone.RelationalModel.extend({
	idAttribute: 'id',

	relations: [{
		type: Backbone.HasOne,
		key: "recipe",
		relatedModel: "Recipe",
		ReverseRelation: {
			key: "stock_id",
			includeInJSON: "id"
		}
	}],
	
	defaults: {
		quantity: 0,
		shelf_life: ""
	},

	url: function() {
		return Config.baseURL + "stock/" + this.id;
	}
});

var Stocks = Backbone.Collection.extend({
	model: Stock,
	url: function() {
		return Config.baseURL + "stock/";
	}
});
