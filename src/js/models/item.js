/**
 * Item BackboneJS model for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var Item = Backbone.RelationalModel.extend({
	idAttribute: "id",
	
	defaults: {
		name: "",
		description: "",
		price: 0.00,
		picture: "",
		quantity: 0,
		shelf_life: "",
		units: ""
	},

	url: function() {
		if (this.id) {
			return Config.baseURL + "items/" + this.id;
		} else {
			return Config.baseURL + "items/";
		}
	}
});

var Items = Backbone.Collection.extend({
	model: Item,
	url: function() {
		return Config.baseURL + "items/";
	}
});
