/**
 * Recipe BackboneJS model for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGBPLv3 or later
 *
 **/

var Recipe = Backbone.RelationalModel.extend({
	idAttribute: "id",

	relations: [{
		type: Backbone.HasMany,
		key: "items",
		relatedModel: "RecipeItem",
		reverseRelation: {
			key: "recipe_id",
			includeInJSON: "cid"
		}
	},{
		type: Backbone.HasMany,
		key: "prices",
		relatedModel: "RecipePrice",
		reverseRelation: {
			key: "recipe_id",
			includeInJSON: "cid"
		}
	}],
	
	defaults: {
		name: "",
		recipe_yield: 0,
		shelf_life: "",
		units: "",
		production_time: 0.00,
		prices: []
	},

	url: function() {
		return Config.baseURL + "recipes/" + this.id;
	}
});

var Recipes = Backbone.Collection.extend({
	model: Recipe,
	url: function() {
		return Config.baseURL + "recipes/";
	}
});
