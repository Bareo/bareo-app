/**
 * RecipeItem BackboneJS modelfor Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var RecipeItem = Backbone.RelationalModel.extend({
	idAttribute: "cid",
	
	defaults: {
		id: 0,
		quantity: 0.0,
		units: ""
	},

	url: function() {
		return false;
	}
});

var RecipeItems = Backbone.Collection.extend({
	model: RecipeItem,
	url: function() {
		return false;
	}
});
