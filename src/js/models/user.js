/**
 * User BackboneJS model for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var User = Backbone.RelationalModel.extend({
	idAttribute: "id",

	// A User has many Account child nodes
	relations: [{
		type: Backbone.HasMany,
		key: "accounts",
		relatedModel: "Account",
		reverseRelation: {
			key: "user_id",
			includeInJSON: "id"
		}
	}],
	
	defaults: {
		username: "",
		email: "",
		phone: "",
		privilege: 0,
		auth_token: ""
	},
	url: function() {
		if (this.id) {
			return Config.baseURL + "users/" + this.id;
		} else {
			return Config.baseURL + "users/";
		}
	},

	initialize: function(options) {
		_.bindAll(this,
		          "auth",
		          "deauth"
		          );
	},
	
	auth: function(password) {
		var auth_url = Config.baseURL + "auth/";
		var body = '{"username":"' + this.get("username") +
			    '","password":"' + password + '"}';
		var xhr = Config.createCORSRequest('POST', auth_url);

		xhr.onload = _.bind(function() {
			var resp_obj = JSON.parse(xhr.responseText);
			this.set(resp_obj);
			this.fetch();
			this.trigger("change");
		},this);
		
		xhr.send(body);
	},

	deauth: function() {
		// For some reason, CORS is still blocking this request.
		// However, since everything is JS only, refreshing the page has the
		// effect of logging the user out.
		
		var auth_url = Config.baseURL + "auth/" + this.get("id");
		var xhr = Config.createCORSRequest('DELETE', auth_url);

		xhr.setRequestHeader('X-Authorization', this.get('auth_token'));
		
		xhr.onload = _.bind(function() {
			var resp_obj = JSON.parse(xhr.responseText);
			this.set(resp_obj);
		},this);

		xhr.send();
		window.location.reload();
	}
});

var Users = Backbone.Collection.extend({
	model: User,
	url: function() {
		return Config.baseURL + "users/";
	}		
});

