/**
 * Menu BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var MenuView = Backbone.View.extend({
	el: "#menu-container",

	template: function() {
		return _.template($("#menu-template").html());
	},

	events: {
		'click .store-link': 'storeView'
	},

	initialize: function(options) {
		_.bindAll(this,
		          "render",
		          "storeView"
		         );
	},

	storeView: function(e) {
		e.preventDefault();
		e.stopPropagation();

		console.log("CLICK HANDLER");
		this.trigger("store");
	},
	
	render: function() {
		//console.log("in menu.render");
		//console.log(this.$el);
		/*this.$el.html(this.template()({
			account_name: this.user.get("username")
		}));
		return this;
		 */
		return this.template()({
			account_name: this.user.get("username")
		});
	}	
});
