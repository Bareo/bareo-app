/**
 * Sale BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var SaleView = Backbone.View.extend({
	tagName: "li",
	
	model: Sale,

	template: function() {
		return _.template($("#sale-template").html());
	},

	/*events: {

	 },*/

	initialize: function(options) {
		this.listenTo(this.model, 'change', this.render);

		_.bindAll(this,
		          "render"
		          );
	},

	render: function() {
		var sale_str = "";
		if (this.model.get("objects").recipes.length) {
			_.each(this.model.get("objects").recipes, function(sale_recipe) {
				sale_str += (sale_str === "" ? "" : ", ") +
					(sale_recipe.quantity + " " + sale_recipe.recipe.name);
			});
		}
		if (this.model.get("objects").items.length) {
			_.each(this.model.get("objects").items, function(sale_item) {
				sale_str += (sale_str === "" ? "" : ", ") +
					(sale_item.quantity + " " + sale_item.item.name);
			});
		}
		
		this.$el.html(this.template()(_.extend(this.model.toJSON(),
		                                       {disp_objects: sale_str})));

		return this;
	}
});
