/**
 * RecipeModal BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo LLC <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 */

var RecipeModalView = Backbone.View.extend({
	className: "recipe-card card",

	model: Recipe,

	template: function() {
		return _.template($("#recipe-modal-template").html());
	},

	initialize: function(options) {
		this.listenTo(this.model, 'change', this.render);

		_.bindAll(this,
		          'render');
	},

	render: function() {
		this.$el.html(this.template()(this.model.toJSON()));
		return this;
	}
});
