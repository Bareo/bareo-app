/**
 * Recipe BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var RecipeView = Backbone.View.extend({
	className: "recipe-card card minimized",

	model: Recipe,

	template: function() {
		return _.template($("#recipe-template").html());
	},

	events: {
		'click .card-header': 'toggleCardView',
		'click .card-body': 'toggleCardView',
		'click .card-footer': 'toggleCardView'
	},

	initialize: function(options) {
		this.listenTo(this.model, 'change', this.render);

		_.bindAll(this,
		          "render",
		          "toggleCardView"
		         );
	},

	render: function() {
		this.$el.html(this.template()(this.model.toJSON()));
		return this;
	},

	toggleCardView: function(e) {
		e.preventDefault();
		e.stopPropagation();

		var recipemodal = new RecipeModalView({ model: this.model });
		app.renderModalView(recipemodal);
	}
});
