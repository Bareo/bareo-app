/**
 * Stock BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 */

var StockView = Backbone.View.extend({
	tagName: "div",

	model: Stock,

	template: function() {
		return _.template($("#stock-template").html());
	},

	/*events: {

	 },*/

	initialize: function(options) {
		this.listenTo(this.model, 'change', this.render);

		_.bindAll(this,
		          "render"
		          );
	},

	render: function() {
		this.$el.html(this.template()(this.model.toJSON()));
	}
});
