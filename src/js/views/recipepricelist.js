/**
 * RecipePriceList BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var RecipePriceListView = Backbone.View.extend({
	template: function() {
		return _.template($("#recipe-price-list-template").html());
	},

	events: {

	},

	initialize: function(options) {
		this.recipePrices = new RecipePrices();

		this.listenTo(this.recipePrices, 'change', this.render);

		_.bindAll(this,
		          "render",
		          "addOneRecipePrice",
		          "addAllRecipePrices"
		          );
	},

	addOneRecipePrice: function(recipePrice) {
		var view = new RecipePriceView({ model: recipePrice });
		this.$('.recipe-price-list').append(view.render().el);
	},
	
	addAllRecipePrices: function() {
		this.recipePrices.each(this.addOneRecipePrice, this);
	},

	render: function() {
		this.$el.html(this.template());
		this.addAllRecipePrices();
		return this;
	}
});
