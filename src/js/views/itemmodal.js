/**
 * ItemModal BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo LLC <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 */

var ItemModalView = Backbone.View.extend({
	className: "item-card card",

	model: Item,

	template: function() {
		return _.template($("#item-modal-template").html());
	},

	initialize: function(options) {
		this.listenTo(this.model, 'change', this.render);

		_.bindAll(this,
		          'render');
	},

	render: function() {
		this.$el.html(this.template()(this.model.toJSON()));
		return this;
	}
});
