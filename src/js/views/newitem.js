
/**
 * NewItem BackboneJs view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 */

var NewItemView = Backbone.View.extend({
	className: "card",

	model: Item,

	template: function() {
		return _.template($("#new-item-template").html());
	},

	events: {
		'submit #new-item-form': 'createNewItem'
	},

	initialize: function(user) {
		this.user = user;

		_.bindAll(this,
		          "render",
		          "createNewItem"
		          );
	},

	render: function() {
		this.$el.html(this.template());
		return this;
	},

	createNewItem: function(e) {
		e.preventDefault();
		e.stopPropagation();

		// Validate the form inputs
		var itemquantity = this.$("input[name='item-quantity']").val();
		var itemunits = this.$("input[name='item-units']").val();
		var itemname = this.$("input[name='item-name']").val();
		var itemprice = this.$("input[name='item-price']").val();
		var itemshelflife = this.$("input[name='item-shelf-life']").val();
		var itemsupplier = this.$("input[name='item-supplier']").val();
		var itemsupplieremail = this.$("input[name='item-supplier-email']").val();

		var newItem = new Item({
			name: itemname,
			description: "",
			price: itemprice,
			picture: "",
			quantity: itemquantity,
			shelf_life: itemshelflife,
			units: itemunits,
			account_id: this.user.get("accounts").first().get("id")
		});

		newItem.save({},{
			success: _.bind(function(model, response, options) {
				console.log("triggering done");
				this.trigger('done');
			}, this)
		});

		this.trigger('close_modal');
	}
});
