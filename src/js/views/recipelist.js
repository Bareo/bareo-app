/**
 * RecipeList BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 */

var RecipeListView = Backbone.View.extend({
	el: "#recipes-container",

	template: function() {
		return _.template($("#recipes-template").html());
	},

	events: {
		'click #new-recipe-button': 'newRecipe'
	},

	initialize: function(options) {
		this.recipes = new Recipes();

		this.listenTo(this.recipes, 'sync', this.render);

		_.bindAll(this,
		          "render",
		          "addOneRecipe",
		          "addAllRecipes",
		          "getAllRecipes",
		          "newRecipe"
		         );
	},

	getAllRecipes: function() {
		this.recipes.fetch({
			success: _.bind(function() {
				this.fetchedRecipes = this.recipes.models[0].attributes.recipes;
				this.recipes.reset(this.fetchedRecipes);
				this.trigger("update");
			},this)
		});
	},

	newRecipe: function(e) {
		e.preventDefault();
		e.stopPropagation();

		this.trigger("new_recipe");
	},

	addOneRecipe: function(recipe) {
		var view = new RecipeView({ model: recipe });
		this.$('.recipe-list').append(view.render().el);
	},

	addAllRecipes: function() {
		this.recipes.each(this.addOneRecipe, this);
	},

	render: function() {
		this.$el.html(this.template());
		this.addAllRecipes();
		return this;
	}
});

