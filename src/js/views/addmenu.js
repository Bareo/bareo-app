/**
 * AddMenu BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo LLC <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var AddMenuView = Backbone.View.extend({
	className: "card",
	
	template: function() {
		return _.template($("#add-menu-template").html());
	},

	events: {
		'click .add-item':   'addItem',
		'click .add-recipe': 'addRecipe',
		'click .add-stock':  'addStock',
		'click .add-sale':   'addSale'
	},

	initialize: function(options) {
		_.bindAll(this,
		          "render",
		          "addItem",
		          "addRecipe",
		          "addStock",
		          "addSale"
		         );
	},

	render: function() {
		this.$el.html(this.template()());
		return this;
	},

	addItem: function(e) {
		e.preventDefault();
		e.stopPropagation();
		this.trigger('new_item');
	},
		
	addRecipe: function(e) {
		e.preventDefault();
		e.stopPropagation();
		this.trigger('new_recipe');
		alert("new recipe");
	},
		
	addStock: function(e) {
		e.preventDefault();
		e.stopPropagation();
		this.trigger('new_stock');
		alert("new stock");
	},
		
	addSale: function(e) {
		e.preventDefault();
		e.stopPropagation();
		this.trigger('new_sale');
		alert("new sale");
	}	
});
