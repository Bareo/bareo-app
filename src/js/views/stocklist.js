/**
 * StockList BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var StockListView = Backbone.View.extend({
	el: "#stocks-container",

	template: function() {
		return _.template($("#stocks-template").html());
	},

	events: {
		'click #new-stock-button': 'newStock'
	},

	initialize: function(options) {
		this.stocks = new Stocks();
		
		this.listenTo(this.stocks, 'sync', this.render);

		_.bindAll(this,
		          "render",
		          "addOneStock",
		          "addAllStocks",
		          "getAllStocks",
		          "newStock"
		          );
	},

	getAllStocks: function() {
		this.stocks.fetch({
			success: _.bind(function() {
				this.fetchedStocks = this.stocks.models[0].attributes.stock;
				this.stocks.reset(this.fetchedStocks);
				//console.log(this.stocks);
				this.trigger("update");
			},this)
		});
	},

	newStock: function(e) {
		e.preventDefault();
		e.stopPropagation();

		this.trigger("new_stock");
	},
	
	addOneStock: function(stock) {
		var view = new StockView({ model: stock });
		this.$('.stock-list').append(view.render().el);
	},

	addAllStocks: function() {
		this.stocks.each(this.addOneStock, this);
	},

	render: function() {
		this.$el.html(this.template());
		this.addAllStocks();
		return this;
	}
});
