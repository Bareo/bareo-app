/**
 * Item BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var ItemView = Backbone.View.extend({
	className: "item-card card minimized",

	model: Item,

	template: function() {
		return _.template($("#item-template").html());
	},

	events: {
		'click .card-header': 'toggleCardView',
		'click .card-body': 'toggleCardView',
		'click .card-footer': 'toggleCardView'
	},

	initialize: function(options) {
		this.listenTo(this.model, 'change', this.render);

		_.bindAll(this,
		          "render",
		          "toggleCardView"
		         );
	},

	render: function() {
		this.$el.html(this.template()(this.model.toJSON()));
		return this;
	},

	toggleCardView: function(e) {
		e.preventDefault();
		e.stopPropagation();

		// Put another copy of this view in a modal
		var itemmodal = new ItemModalView({ model: this.model });
		app.renderModalView(itemmodal);
	}
});
