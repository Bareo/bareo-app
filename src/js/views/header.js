/**
 * Header BackboneJS view for Bareo web client
 * 
 * Copyright 2015 zachwick and Bareo LLC <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var HeaderView = Backbone.View.extend({

	template: function() {
		return _.template($("#header-template").html());
	},

	events: {
		'click .logout': 'logoutHandler',
		'click .forecasting': 'showForecastingPane',
		'click .pantry': 'showPantryPane'
	},

	initialize: function(options) {
		_.bindAll(this,
		          'render',
		          'logoutHandler',
			  'showForecastingPane',
			  'showPantryPane'
		         );
	},

	render: function() {
		this.$el.html(this.template()({
			username: this.user.get("username"),
			app_url: window.location.href
		}));
		return this;
	},

	logoutHandler: function(e) {
		e.preventDefault();
		e.stopPropagation();

		this.trigger("logout");
	},

	showPantryPane: function(e) {
		e.preventDefault();
		e.stopPropagation();

		this.trigger('show_pantry');
	},

	showForecastingPane: function(e) {
		e.preventDefault();
		e.stopPropagation();

		this.trigger('show_forecasting');
	}
});
