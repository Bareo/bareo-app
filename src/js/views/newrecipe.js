/**
 * NewRecipe BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var NewRecipeView = Backbone.View.extend({
	className: "card",

	model: Recipe,

	template: function() {
		return _.template($("#new-recipe-template").html());
	},

	events: {
		'submit #new-recipe-form': 'createNewRecipe'
	},

	initialize: function(user) {
		this.user = user;

		_.bindAll(this,
		          "render",
		          "createNewRecipe"
		          );
	},

	render: function() {
		this.$el.html(this.template());
		return this;
	},

	createNewRecipe: function(e) {
		e.preventDefault();
		e.stopPropagation();

		// Validate the form inputs
		var recipeName = this.$("input[name='recipe-name']").val();
		var recipeShelfLife = this.$("input[name='recipe-shelf-life']").val();
		var recipeYield = this.$("input[name='recipe-yield']").val();
		var recipeProductionTime = this.$("input[name='recipe-production-time']").val();
		var recipeUnits = this.$("input[name='recipe-units']").val();

		var newRecipe = new Recipe({
			// TODO: zwick Finish implementing this
		});
	}
});
