/**
 * Footer BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo LLC <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var FooterView = Backbone.View.extend({

	template: function() {
		return _.template($("#footer-template").html());
	},

	events: {
		'click .add-menu': 'addMenuHandler'
	},

	initialize: function(options) {
		_.bindAll(this,
		          "render",
		          "addMenuHandler"
		         );
	},

	render: function() {
		this.$el.html(this.template()());
		return this;
	},

	addMenuHandler: function(e) {
		e.preventDefault();
		e.stopPropagation();

		this.trigger('add_menu');
	}
});
