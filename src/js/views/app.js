/**
 * App BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var AppView = Backbone.View.extend({
	// A Sizzle/jQuery selector for the DOM element that an AppView object
	// uses to interact with the DOM
	el: "#app-container",

	// Note that this is a fancy way to wrap around the UnderscoreJS template
	// function. We will use this AppView.template function to manipulate the
	// DOM
	template: function() {
		return _.template($("#app-template").html());
	},

	// Define any UI events and the functions that are bound to them
	events: {
		'submit #login-form': 'loginUser',
		'submit #signup-form': 'signupUser',
		'click .x-button': 'closeModalView',
		'click #new_account': 'showSignupForm'
	},

	// This AppView.initialize function is called whenever we create a new
	// AppView object; Think of it kind of like a C++ constructor
	initialize: function(options) {
		this.user        = new User();
		this.menu        = new MenuView();
		this.header      = new HeaderView();
		this.footer      = new FooterView();
		this.addMenu     = new AddMenuView();
		this.menu.user   = this.user;
		this.header.user = this.user;
		this.newItem     = new NewItemView(this.user);

		this.signupsuccess = new SignupSuccess();

		this.saleList   = new SaleListView();
		this.itemList   = new ItemListView();
		this.recipeList = new RecipeListView();
		this.stockList  = new StockListView();

		this.listenTo(this.user, 'change', this.render);

		// The following custom events come from selecting menu items
		this.listenTo(this.menu, 'main', this.renderPantryViews);
		this.listenTo(this.menu, 'store', this.renderStoreViews);
		this.listenTo(this.menu, 'items', this.renderItemViews);
		this.listenTo(this.menu, 'recipes', this.renderRecipeViews);
		this.listenTo(this.menu, 'sales', this.renderSaleViews);

		this.listenTo(this.header, 'logout', this.logoutUser);
		this.listenTo(this.header, 'show_pantry', this.renderPantryViews);
		this.listenTo(this.header, 'show_forecasting', this.renderForecastingViews);
		this.listenTo(this.footer, 'add_menu', this.showAddMenu);

		this.listenTo(this.addMenu, 'new_item', this.renderNewItemView);

		this.listenTo(this.newItem, 'done', this.renderPantryViews);
		this.listenTo(this.newItem, 'close_modal', this.closeModalView);

		// Ensure that 'this' plays nicely inside each of our AppView's methods
		_.bindAll(this,
		          "appendToContent",
		          "loginUser",
		          "logoutUser",
		          "signupUser",
		          "render",
		          "renderContent",
		          "renderPantryViews",
		          "renderStoreViews",
		          "renderItemViews",
		          "renderRecipeViews",
		          "renderSaleViews",
		          "renderNewItemView",
		          "renderMenu",
		          "renderHeader",
		          "renderFooter",
			  "renderForecastingViews",
		          "fetchAllResources",
		          "renderModalView",
		          "closeModalView",
		          "showAddMenu",
		          "showSignupForm",
			  "stripeResponseHandler"
		         );

		this.render();
	},

	// This method exists so that post-login we can fetch all items, recipes,
	// sales, etc. from one entry point.
	fetchAllResources: function() {
		this.saleList.getAllSales();
		this.itemList.getAllItems();
		this.recipeList.getAllItems();
		this.stockList.getAllStocks();
	},

	loginUser: function(e) {
		e.preventDefault();
		e.stopPropagation();

		var passwd = this.$("input[name='password']").val();

		this.user.set({ username: this.$("input[name='username']").val()});
		this.user.auth(passwd);
	},

	logoutUser: function() {
		this.user.deauth();
	},

	signupUser: function(e) {
		e.preventDefault();
		e.stopPropagation();

		// First, submit payment details to Stripe to get a token
		//Stripe.setPublishableKey('pk_test_z0nG6x8ur5ovB75UMrSg6Xvo');
		Stripe.setPublishableKey('pk_live_fGL6mEFzAJf7Prew2QGH67IX');
		Stripe.card.createToken({
			number:      this.$("input[name='su_pan']").val(),
			cvc:         this.$("input[name='su_cvc']").val(),
			exp_month:   this.$("input[name='su_exp_month']").val(),
			exp_year:    this.$("input[name='su_exp_year']").val(),
			address_zip: this.$("input[name='su_zip']").val()
		}, this.stripeResponseHandler);
	},

	stripeResponseHandler: function(status, response) {
		var su_username = this.$("input[name='su_username']").val();
		var su_password = this.$("input[name='su_password']").val();
		var su_email    = this.$("input[name='su_email']").val();
		var su_phone    = this.$("input[name='su_phone']").val();
		var su_privilege = 1;

		var newUser = new User({
			username:  su_username,
			password:  su_password,
			email:     su_email,
			privilege: su_privilege,
			phone:     su_phone,
			token:     response.id
		});
		newUser.save({},{
			success: _.bind(function(model, response, options) {
				// HACK: For some reason, the user:change event is not getting
				//       caught. This has the effect of giving absolutely no
				//       feedback to the user that their signup was successful.
				//       So, lets just show a success modal where clicking the
				//       okay button takes them to the login page.
				this.renderModalView(this.signupsuccess);
				//this.user = newUser;
				//this.user.auth(su_password);
			},this)
		});
	},
	// This method is the access point of all DOM manipulation by the
	// AppView object
	render: function() {
		var loggedIn = !!(this.user.get("auth_token"));

		this.$el.html(this.template()({
			loggedIn: loggedIn
		}));

		if (loggedIn) {
			//this.renderMenu();
			this.renderHeader();
			this.renderFooter();
			this.renderPantryViews();
		}
	},

	showSignupForm: function(e) {
		e.preventDefault();
		e.stopPropagation();
		this.$("#login-form-right").hide();
		this.$("#signup-form-right").show();
	},

	renderMenu: function() {
		this.$("#menu-container").html(this.menu.render());
	},

	renderHeader: function() {
		this.header.setElement(this.$('#header-container'));
		this.header.render();
	},

	renderFooter: function() {
		this.footer.setElement(this.$('#footer-container'));
		this.footer.render();
	},

	appendToContent: function(view) {
		//view.setElement("#content-container");
		view.render();
		this.$("#content-container").append(view);

	},

	renderContent: function(view) {
		view.render();
	},

	renderPantryViews: function(e) {
		// Ensure that we have the latest data
		this.itemList.getAllItems();
		this.recipeList.getAllRecipes();
		this.stockList.getAllStocks();

		// Render the Stocks view
		this.$("#content-container").append(this.stockList.render().$el);
		this.$("#content-container #stocks-container").show();

		// Render the Recipes view
		this.$("#content-container").append(this.recipeList.render().$el);
		this.$("#content-container #recipes-container").show();

		// Render the Items view
		this.$("#content-container").append(this.itemList.render().$el);
		this.$("#content-container #items-container").show();
	},

	renderForecastingViews: function() {
		alert('Showing forecasting data here');
	},

	renderStoreViews: function(e) {
		this.saleList.getAllSales();
		this.itemList.getAllItems();
		this.stockList.getAllStocks();
		this.renderContent(this.stockList);
		//this.renderLeftCol(this.itemList);
		//this.renderRightCol(this.saleList);
	},

	renderItemViews: function(e) {
		this.itemList.getAllItems();
		this.renderContent(this.itemList);
	},

	renderRecipeViews: function(e) {
		this.recipeList.getAllRecipes();
		this.renderContent(this.recipeList);
	},

	renderSaleViews: function(e) {
		this.saleList.getAllSales();
		this.renderContent(this.saleList);
	},

	renderNewItemView: function() {
		this.closeModalView();
		this.renderModalView(this.newItem);
	},

	renderModalView: function(view) {
		this.$("#modal-background").show();
		this.$("#modal-container").show();
		view.setElement(this.$("#modal-container .modal-content"));
		view.render();
	},

	showAddMenu: function() {
		this.renderModalView(this.addMenu);
		this.$("#modal-container").addClass("narrow");
	},

	closeModalView: function(e) {
		if (e !== undefined) {
			e.preventDefault();
			e.stopPropagation();
		}
		this.$("#modal-background").hide();
		this.$("#modal-container .modal-content").empty();
		this.$("#modal-container").removeClass("narrow");
		this.$("#modal-container").hide();
	}
});
