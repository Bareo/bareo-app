/**
 * ItemList BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var ItemListView = Backbone.View.extend({
	el: "#items-container",

	template: function() {
		return _.template($("#items-template").html());
	},

	events: {
		'click #new-item-button': 'newItem'
	},

	initialize: function(options) {
		this.items = new Items();

		this.listenTo(this.items, 'sync', this.render);

		_.bindAll(this,
		          "render",
		          "addOneItem",
		          "addAllItems",
		          "getAllItems"
		         );
	},

	getAllItems: function() {
		this.items.fetch({
			success: _.bind(function() {
				this.fetchedItems = this.items.models[0].attributes.items;
				this.items.reset(this.fetchedItems);
				this.trigger("update");
			},this)
		 });
	},

	newItem: function(e) {
		e.preventDefault();
		e.stopPropagation();

		this.trigger("new_item");
	},

	addOneItem: function(item) {
		var view = new ItemView({ model: item });
		this.$(".item-list").append(view.render().el);
	},

	addAllItems: function() {
		this.items.each(this.addOneItem, this);
	},

	render: function() {
		this.$el.html(this.template());
		this.addAllItems();
		return this;
	}
});
