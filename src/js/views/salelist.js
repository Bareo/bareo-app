/**
 * SaleList BackboneJS view for Bareo web client
 *
 * Copyright 2015 zachwick and Bareo <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 **/

var SaleListView = Backbone.View.extend({
	el: "#sales-container",
	
	template: function() {
		return _.template($("#sales-template").html());
	},

	events: {
		'click #new-sale-button': 'newSale'
	},

	initialize: function(options) {
		this.sales = new Sales();
		
		this.listenTo(this.sales, 'sync', this.render);

		_.bindAll(this,
		          "render",
		          "addOneSale",
		          "addAllSales",
		          "getAllSales"
		          );
	},

	getAllSales: function() {	
		this.sales.fetch({
			success: _.bind(function() {
				this.fetchedSales = this.sales.models[0].attributes.sales;
				this.sales.reset(this.fetchedSales);
				this.trigger("update");
			},this)
		});
	},
	
	newSale: function(e) {
		e.preventDefault();
		e.stopPropagation();

		alert("CREATE NEW SALE");
	},

	addOneSale: function(sale) {
		var view = new SaleView({ model: sale });
		if (sale.get("status") == 0 ||
		    sale.get("status") == 1 ||
		    sale.get("status") == 2) {
			// 0 == new sale
			// 1 == needs one or ingredients
			// 2 == needs made
			this.$(".pending-sales-list").append(view.render().el);			
		} else if (sale.get("status") == 3 || sale.get("status") == 4) {
			// 3 == sale completed but not paid for
			// 4 == sale completed and paid for, but not picked up
			this.$(".in-production-sales-list").append(view.render().el);
		} else if (sale.get("status") == 5) {
			// 5 == sale completed, paid for, and picked up
			this.$(".completed-sales-list").append(view.render().el);			
		}
	},

	addAllSales: function() {
		this.sales.each(this.addOneSale, this);
	},

	render: function() {
		this.$el.html(this.template());
		this.addAllSales();
	}
});
