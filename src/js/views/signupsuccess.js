/**
 * SignupSuccess BackboneJS view for Bareo web client
 *
 * Copyright 2016 zachwick and Bareo LLC <zwick@bareo.io>
 * Licensed under the GNU AGPLv3 or later
 *
 */

var SignupSuccess = Backbone.View.extend({
	className: "card",

	model: User,

	template: function() {
		return _.template($("#signup-success-template").html());
	},

	events: {
		'click .modal-button': 'goToLogin'
	},

	initialize: function() {

		_.bindAll(this,
		          "render",
		          "goToLogin"
		          );
	},

	render: function() {
		this.$el.html(this.template());
		return this;
	},

	goToLogin: function(e) {
		e.preventDefault();
		e.stopPropagation();

		// Dump the user back to the login page
		window.location = Config.appURL;
	}
});
