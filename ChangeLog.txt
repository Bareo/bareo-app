2015-10-01  zachwick <zach@zachwick.com>

	* docs/views/header.html, index.html, src/js/views/header.js,
	  src/less/app.less, static/app.css, static/bareo-app.js: Makes
	  header links actually work
	* docs/views/app.html, docs/views/header.html, docs/views/item.html,
	  docs/views/itemlist.html, docs/views/itemmodal.html,
	  docs/views/menu.html, docs/views/newitem.html,
	  docs/views/recipe.html, docs/views/recipelist.html,
	  docs/views/recipemodal.html, docs/views/sale.html,
	  docs/views/salelist.html, docs/views/stock.html,
	  docs/views/stocklist.html, index.html, src/js/views/app.js,
	  src/js/views/header.js, src/less/app.less, static/app.css, static
	  /bareo-app.js: Begins implementing app header
	* docs/models/recipe.html, docs/views/app.html, docs/views/item.html,
	  docs/views/itemlist.html, docs/views/itemmodal.html,
	  docs/views/menu.html, docs/views/newitem.html,
	  docs/views/recipe.html, docs/views/recipelist.html,
	  docs/views/recipemodal.html, docs/views/sale.html,
	  docs/views/salelist.html, docs/views/stock.html,
	  docs/views/stocklist.html, index.html, src/js/models/recipe.js,
	  src/js/views/item.js, src/js/views/recipe.js,
	  src/js/views/recipemodal.js, static/bareo-app.js: Recipe views are
	  now working correctly
	* src/less/app.less, static/app.css: Fixes some viewport sizing CSS
	* docs/views/app.html, docs/views/item.html, index.html,
	  src/js/views/app.js, src/js/views/item.js, src/less/app.less,
	  static/app.css, static/bareo-app.js: Making an Item modal works
	* docs/views/app.html, docs/views/item.html,
	  docs/views/itemlist.html, docs/views/itemmodal.html,
	  docs/views/menu.html, docs/views/newitem.html,
	  docs/views/recipe.html, docs/views/recipelist.html,
	  docs/views/sale.html, docs/views/salelist.html,
	  docs/views/stock.html, docs/views/stocklist.html, index.html,
	  src/js/views/item.js, src/js/views/itemmodal.js, static/bareo-
	  app.js: Begins working on styling modals
	* docs/views/app.html, docs/views/item.html, src/js/views/app.js,
	  src/js/views/item.js, src/less/app.less, static/app.css, static
	  /bareo-app.js: Working on expanding a card
	* docs/views/app.html, src/js/views/app.js, src/less/app.less,
	  static/app.css, static/bareo-app.js: More card styling

2015-09-30  zachwick <zach@zachwick.com>

	* docs/views/app.html, index.html, src/js/views/app.js,
	  src/less/app.less, static/app.css, static/bareo-app.js: More
	  item/recipe listing
	* docs/views/app.html, docs/views/itemlist.html, index.html,
	  src/js/views/app.js, src/js/views/itemlist.js, static/bareo-app.js:
	  Listing recipes now works
	* docs/views/app.html, docs/views/item.html, docs/views/recipe.html,
	  index.html, src/js/views/app.js, src/js/views/item.js,
	  src/js/views/recipe.js, src/less/app.less, static/app.css, static
	  /bareo-app.js: Implements card views for Items
	* docs/views/app.html, docs/views/item.html, docs/views/recipe.html,
	  docs/views/recipelist.html, docs/views/stock.html,
	  docs/views/stocklist.html, index.html, src/js/views/app.js,
	  src/js/views/item.js, src/js/views/recipe.js,
	  src/js/views/recipelist.js, src/js/views/stock.js,
	  src/js/views/stocklist.js, src/less/app.less, static/app.css,
	  static/bareo-app.js: More re-working of the UI
	* docs/views/app.html, docs/views/menu.html,
	  docs/views/recipelist.html, index.html, src/js/views/app.js,
	  src/js/views/menu.js, src/js/views/recipelist.js, static/bareo-
	  app.js: Makes RecipeListView work

2015-09-28  zachwick <zach@zachwick.com>

	* docs/views/stocklist.html, index.html, src/js/views/stocklist.js,
	  static/bareo-app.js: Working on restructuring the HTML
	* docs/models/account.html, docs/models/docco.css,
	  docs/models/item.html, docs/models/recipe.html,
	  docs/models/sale.html, docs/models/stock.html,
	  docs/models/user.html, docs/views/app.html, docs/views/docco.css,
	  docs/views/item.html, docs/views/itemlist.html,
	  docs/views/menu.html, docs/views/newitem.html,
	  docs/views/recipe.html, docs/views/recipelist.html,
	  docs/views/sale.html, docs/views/salelist.html,
	  docs/views/stock.html, docs/views/stocklist.html: Updates docs

2015-09-24  zachwick <zach@zachwick.com>

	* docs/models/account.html, docs/models/docco.css,
	  docs/models/item.html, docs/models/recipe.html,
	  docs/models/sale.html, docs/models/stock.html,
	  docs/models/user.html, docs/views/app.html, docs/views/docco.css,
	  docs/views/item.html, docs/views/itemlist.html,
	  docs/views/menu.html, docs/views/newitem.html,
	  docs/views/recipe.html, docs/views/recipelist.html,
	  docs/views/sale.html, docs/views/salelist.html,
	  docs/views/stock.html, docs/views/stocklist.html: Updates docs

2015-09-23  zachwick <zach@zachwick.com>

	* src/js/bootstrap.js, static/bareo-app.js: Actual fix for
	  bootstrapping the app
	* src/js/bootstrap.js, static/bareo-app.js: Fixes an app
	  bootstrapping issue
	* docs/models/account.html, docs/models/docco.css,
	  docs/models/item.html, docs/models/public/fonts/roboto-black.eot,
	  docs/models/public/fonts/roboto-black.ttf, docs/models/public/fonts
	  /roboto-black.woff, docs/models/recipe.html, docs/models/sale.html,
	  docs/models/stock.html, docs/models/user.html, docs/views/app.html,
	  docs/views/docco.css, docs/views/item.html,
	  docs/views/itemlist.html, docs/views/menu.html,
	  docs/views/newitem.html, docs/views/public/fonts/roboto-black.eot,
	  docs/views/public/fonts/roboto-black.ttf, docs/views/public/fonts
	  /roboto-black.woff, docs/views/recipe.html,
	  docs/views/recipelist.html, docs/views/sale.html,
	  docs/views/salelist.html, docs/views/stock.html,
	  docs/views/stocklist.html: Updates docs
	* Gruntfile.js, index.html, src/js/lib/bootstrap.js,
	  src/js/lib/npm.js, static/app.css, static/bareo-app.js, static
	  /bootstrap-theme.css, static/bootstrap.css: Adds Twitter Bootstrap
	* Gruntfile.js, src/js/lib/backbone-relational.js,
	  src/js/lib/backbone.js, src/js/lib/jquery-2.1.3.js,
	  src/js/lib/jquery-2.1.4.js, src/js/lib/underscore.js, static/bareo-
	  app.js: Updates JS libs  Updates the following JS libs to the their
	  latest stable versions: - backbone - underscore - jquery -
	  backbone-relational

2015-08-30  zachwick <zach@zachwick.com>

	* docs/models/recipe.html, docs/models/stock.html,
	  src/js/models/recipe.js, src/js/models/stock.js, static/bareo-
	  app.js: Fixes some API URL
	* src/js/config.js, static/bareo-app.js: Enables speaking to dev API
	* docs/models/account.html, docs/models/item.html,
	  docs/models/recipe.html, docs/models/sale.html,
	  docs/models/stock.html, docs/models/user.html, docs/views/app.html,
	  docs/views/item.html, docs/views/itemlist.html,
	  docs/views/menu.html, docs/views/newitem.html,
	  docs/views/recipe.html, docs/views/recipelist.html,
	  docs/views/sale.html, docs/views/salelist.html,
	  docs/views/stock.html, docs/views/stocklist.html, index.html,
	  src/js/models/stock.js, src/js/views/app.js,
	  src/js/views/recipelist.js, src/js/views/stock.js,
	  src/js/views/stocklist.js, static/bareo-app.js: working on
	  implementing /stocks/ support
	* index.html, src/js/views/salelist.js, static/bareo-app.js:
	  Completes changing Order -> Sale
	* src/js/views/app.js, static/bareo-app.js: fixes a typo
	* docs/models/item.html, docs/models/user.html, docs/views/app.html,
	  docs/views/item.html, docs/views/itemlist.html,
	  docs/views/menu.html, docs/views/newitem.html,
	  docs/views/order.html, docs/views/orderlist.html,
	  docs/views/recipe.html, docs/views/recipelist.html,
	  docs/views/sale.html, docs/views/salelist.html,
	  src/js/views/app.js, src/js/views/order.js,
	  src/js/views/orderlist.js, src/js/views/sale.js,
	  src/js/views/salelist.js, static/bareo-app.js: Updates Order ->
	  Sale
	* index.html, src/less/app.less, src/less/config.less,
	  static/bootstrap.css: Moar login/signup work
	* index.html, src/less/app.less, src/less/config.less,
	  static/bootstrap.css, static/images/instagram_logo.png,
	  static/images/signup_extendedback.jpg,
	  static/images/tumblr_logo.png, static/images/twitter_logo.png: Re-
	  working login/signup page

2015-08-28  zachwick <zach@zachwick.com>

	* ChangeLog.txt, gitlog2changelog.py: Adds a ChangeLog.txt and python
	  changelog generation script

2015-04-20  zachwick <zach@zachwick.com>

	* src/js/config.js, static/bareo-app.js: Adds a URL for bareo.hiphop

2015-04-17  zachwick <zach@zachwick.com>

	* src/js/views/app.js, static/bareo-app.js: Makes Pantry view the
	  default
	* index.html, src/js/views/item.js, src/js/views/itemlist.js,
	  src/js/views/recipe.js, src/js/views/recipelist.js: Implements
	  Recipe views

2015-04-09  zachwick <zach@zachwick.com>

	* reset.html, src/js/bootstrap.js, static/bareo-app.js: Unstyled
	  password reset

2015-03-30  zachwick <zach@zachwick.com>

	* index.html: Ensures that the JS is clearly labelled as free
	* index.html, src/js/bootstrap.js, src/js/models/user.js,
	  src/js/views/app.js, src/js/views/newitem.js, src/less/app.less,
	  src/less/config.less, static/bareo-app.js, static/bootstrap.css,
	  static/images/bareo_text.png, static/images/red_login_text.png,
	  static/images/red_signup_text.png: New sign in/up page works

2015-03-16  zachwick <zach@zachwick.com>

	* src/js/config.js, src/js/views/newitem.js, static/bareo-app.js:
	  Ensure that the app is pointing at the live API

2015-03-15  zachwick <zach@zachwick.com>

	* src/js/models/item.js, src/js/models/user.js,
	  src/js/views/newitem.js, static/bareo-app.js: Adding new items now
	  works
	* index.html, src/js/config.js, src/js/views/app.js,
	  src/js/views/itemlist.js, src/js/views/newitem.js, static/bareo-
	  app.js: Begin working on adding new items

2015-03-12  zachwick <zach@zachwick.com>

	* src/js/views/app.js, static/bareo-app.js: Make the default, just
	  logged in view, better
	* README.md, src/js/views/app.js, src/js/views/itemlist.js,
	  src/js/views/menu.js, static/bareo-app.js: Many updates

2015-02-14  zachwick <zach@zachwick.com>

	* index.html: Fixes a duplicate id error
	* docs/models/item.html, docs/views/app.html, docs/views/item.html,
	  docs/views/itemlist.html, docs/views/menu.html,
	  docs/views/order.html, docs/views/orderlist.html, index.html,
	  src/js/models/item.js, src/js/views/app.js, src/js/views/item.js,
	  src/js/views/itemlist.js, src/js/views/orderlist.js, static/bareo-
	  app.js: Now shows items
	* docs/index.html: Adds annotated source index file
	* docs/models/account.html, docs/models/docco.css,
	  docs/models/item.html, docs/models/public/fonts/aller-bold.eot,
	  docs/models/public/fonts/aller-bold.ttf, docs/models/public/fonts
	  /aller-bold.woff, docs/models/public/fonts/aller-light.eot,
	  docs/models/public/fonts/aller-light.ttf, docs/models/public/fonts
	  /aller-light.woff, docs/models/public/fonts/novecento-bold.eot,
	  docs/models/public/fonts/novecento-bold.ttf,
	  docs/models/public/fonts/novecento-bold.woff,
	  docs/models/public/stylesheets/normalize.css,
	  docs/models/recipe.html, docs/models/sale.html,
	  docs/models/user.html, docs/views/app.html, docs/views/docco.css,
	  docs/views/menu.html, docs/views/order.html,
	  docs/views/orderlist.html, docs/views/public/fonts/aller-bold.eot,
	  docs/views/public/fonts/aller-bold.ttf, docs/views/public/fonts
	  /aller-bold.woff, docs/views/public/fonts/aller-light.eot,
	  docs/views/public/fonts/aller-light.ttf, docs/views/public/fonts
	  /aller-light.woff, docs/views/public/fonts/novecento-bold.eot,
	  docs/views/public/fonts/novecento-bold.ttf, docs/views/public/fonts
	  /novecento-bold.woff, docs/views/public/stylesheets/normalize.css:
	  Updates annotated source documentation
	* README.md, bareo-app.org: Adds some much needed documentation
	* src/js/config.js, static/bareo-app.js: Use live API instead of dev
	  API
	* bareo-app.org, src/less/app.less, static/bootstrap.css: A little
	  CSS for columns

2015-02-13  zachwick <zach@zachwick.com>

	* src/js/config.js, src/js/views/app.js, src/js/views/order.js,
	  src/js/views/orderlist.js, static/bareo-app.js: Sales/Orders now
	  display
	* index.html, src/js/models/sale.js, src/js/views/app.js,
	  src/js/views/order.js, src/js/views/orderlist.js, static/bareo-
	  app.js: Working on orderlist view rendering
	* index.html, src/js/bootstrap.js, src/js/config.js,
	  src/js/models/sale.js, src/js/models/user.js, src/js/views/app.js,
	  src/js/views/order.js, src/js/views/orderlist.js, static/bareo-
	  app.js: Now fetches orders/sales upon login

2015-02-12  zachwick <zach@zachwick.com>

	* index.html, src/js/views/app.js, src/js/views/menu.js,
	  src/less/app.less, static/bareo-app.js, static/bootstrap.css:
	  Begins to implement left menu
	* src/less/app.less, static/bootstrap.css: Implements basic column
	  layout CSS
	* static/bareo-app.min.js, static/bootstrap.css.gz: Removes outdated
	  files
	* index.html, src/less/app.less, src/less/bootstrap.less,
	  static/bootstrap.css: Begins to style the AppView
	* src/less/config.less: Adds light and dark gray colors
	* src/js/models/user.js, src/js/views/app.js, static/bareo-app.js:
	  Removes leftover debug loggin
	* index.html, src/js/config.js, src/js/models/user.js,
	  src/js/views/app.js, static/bareo-app.js: User login now displayed
	  only when needed

2015-02-10  zachwick <zach@zachwick.com>

	* src/js/models/user.js, static/bareo-app.js: working on user auth
	* src/js/bootstrap.js, src/js/config.js, src/js/models/.#item.js,
	  src/js/models/user.js, src/js/views/app.js, static/bareo-app.js:
	  DEBUG/DEV only use auth now implemented
	* Gruntfile.js, index.html, src/js/bootstrap.js, src/js/config.js,
	  src/js/lib/backbone-relational.js, src/js/lib/backbone.js,
	  src/js/lib/jquery-2.1.3.js, src/js/lib/underscore.js, src/js/libs
	  /backbone-relational.js, src/js/libs/backbone.js,
	  src/js/libs/jquery-2.1.3.js, src/js/libs/underscore.js,
	  src/js/models/.#item.js, src/js/models/account.js,
	  src/js/models/item.js, src/js/models/recipe.js,
	  src/js/models/sale.js, src/js/models/user.js, static/bareo-app.js:
	  Begin working on JS models
	* .gitignore, COPYING, Gruntfile.js, README.md, api.py, app.py,
	  package.json, src/js/bootstrap.js, src/js/libs/backbone-
	  relational.js, src/js/libs/backbone.js,
	  src/js/libs/jquery-2.1.3.js, src/js/libs/underscore.js,
	  src/less/bootstrap.less, src/less/config.less, static/bareo-app.js,
	  static/bareo-app.min.js, static/bootstrap.css,
	  static/bootstrap.css.gz, test.py, tests/lib/qunit/qunit.css,
	  tests/lib/qunit/qunit.js, tests/lib/sinon-1.6.0.js, tests/lib
	  /sinon-qunit-1.0.0.js: Cleaning out the repo for a pure JS webapp
	  build  We no longer need a whole bunch of junk that was present
	  since we are now conversing directly with the Bareo API.
	* cors_test.html: Changes test url to live API
	* cors_test.html, index.html: Initial live CORS test

2015-02-09  zachwick <zach@zachwick.com>

	* README.md, api.py, app.py, index.html: JS app bootstrapping now
	  works.

2014-12-11  zachwick <zach@zachwick.com>

	* api.py, app.py, bareo-app.org, index.html, test.py: Many Updates  I
	  know that that is a crappy commit message, but I forgot to make
	  commits as I was going along.

2014-12-10  zachwick <zach@zachwick.com>

	* app.py, index.html: Basic URL->class routing implemented
	* api.py, app.py: Intestitial commit before switching to python2
	  client for demo

2014-12-06  zachwick <zach@zachwick.com>

	* .gitignore, COPYING, README.md, api.py, app.py, bareo-app.org,
	  test.py: Initial commit of bareo-web app
