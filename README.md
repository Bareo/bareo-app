# bareo-app

bareo-app is a web client for the Bareo API.

## Usage

The easiest way to use the Bareo API web client is to use the [Bareo
hosted instance](https://app.bareo.io/). However, since the web client
uses CORS requests to talk directly to the Bareo API, it is possible
to run the web client off of your own machine instead of off of
app.bareo.io.

To run the Bareo web client locally, it is sufficient to simply point
your browser at the [Bareo hosted version](https://app.bareo.io). Then
right click on the page and select *Save Page As.." and in the
resulting dialog, make sure to select "Web Page, complete". This will
save the HTML file and all of the JS and CSS files that you need for a
fully functioning locally hosted version of the Bareo web client.

Regardless of the licensing of the Bareo web client's source code,
once you running a locally hosted version, you are legally allowed to
modify the HTML, JS, and CSS as you wish. If you make modifications,
you may introduce bugs however. Using the source code in this way
constitutes fair use. You wouldn't be legally allowed to re-distribute
the original downloaded code or your modifications however.

But, the Bareo web client's source code is licensed under the AGPL v3
or later. This means that you are legally allowed to re-distribute
both the originally downloaded code or any modifications that you may
have made. If you do distribute the code however, you have legal
obligations which you can read in the accompanying COPYING file.

If you plan on making modifications, it is probably best to download
the actual source code of the Bareo web client instead of using the
saved Bareo hosted version. See the *Building* section for detailed
instructions.

Once you have a local copy of the Bareo web client, all that you must
do to use it is to open the HTML file in your web browser of
choice. On my machine, I just point surf at
file:///home/zwick/Web_Pages/bareo-saved-copy.html. 

## Building

In order to checkout and build the Bareo web client, you will need the
following installed on your machine:

* git
* nodejs
* npm (may be packaged seperately from nodejs)
* grunt

### Getting the Source

Use git to clone the source code repository

	git clone git://bareo.io:/opt/git/bareo-app.git bareo-app

### Setting Up the Build Environment

Once you have cloned the repository, `cd` into the `bareo-app`
directory. Then install the required npm packages

	npm install

That is really all that there is to set up the build environment.

### Building

In the `bareo-app` directory, once you have installed the required npm
packages, simply run

	grunt

to build a development version with un-minified and un-compressed
resulting files. To see other build options, look at the bottom the `Gruntfile.js`.

### Testing/Running

To run your newly created local copy, simply point your web browser of
choice at the `index.html` file in your `bareo-app` directory.

## Project Structure
- index.hmtl - the HTML page that contains all of the BackboneJS view templates and loads all of the needed resources
- src/ - the JS and LESS source code
- src/js/lib/ - location of 3rd party JS libraries
- src/js/models/ - location of BackboneJS model definitions
- src/js/views/ - location of BackboneJS view definitions

## Contributing

- Bareo doesn't do any kind of Contributor Licensing Agreement
(CLA)
- Make sure that your patchset addresses only one
bug/issue/feature/change
- Please make your code's style match the existing code's style.

